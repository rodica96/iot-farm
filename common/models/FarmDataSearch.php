<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FarmData;

/**
 * FarmDataSearch represents the model behind the search form of `common\models\FarmData`.
 */
class FarmDataSearch extends FarmData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['humidity', 'temperature', 'soil_moisture', 'light'], 'number'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FarmData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orderBy('id desc');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'humidity' => $this->humidity,
            'temperature' => $this->temperature,
            'soil_moisture' => $this->soil_moisture,
            'light' => $this->light,
            'datetime' => $this->datetime,
        ]);

        return $dataProvider;
    }
}
