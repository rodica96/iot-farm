<?php

namespace frontend\controllers;

use common\models\FarmData;
use common\models\FarmDataSearch;
use frontend\helpers\HelperFarm;
use Yii;
use yii\web\Controller;

/**
 * Graph Controller
 */
class FarmController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionHumidity()
    {
        return $this->render('graph',
            [
                'type' => 'humidity',
                'title' => 'Humidity',
                'metric' => '%'
            ]
        );
    }

    public function actionTemperature()
    {
        return $this->render('graph',
            [
                'type' => 'temperature',
                'title' => 'Temperature',
                'metric' => 'Celsius'
            ]
        );
    }

    public function actionSoilMoisture()
    {
        return $this->render('graph',
            [
                'type' => 'soil_moisture',
                'title' => 'Soil Moisture',
                'metric' => '~~'
            ]
        );
    }

    public function actionLight()
    {
        return $this->render('graph',
            [
                'type' => 'light',
                'title' => 'Light',
                'metric' => '~~'
            ]
        );
    }

    public function actionList()
    {
        $searchModel = new FarmDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTest()
    {
        //do some bulk insert
        $rows = 100;

        $last_record = FarmData::find()->orderBy('datetime desc')->limit(1)->all();
        $last_date = $last_record[0]->datetime;
        $start_date = $last_date;

        for ($i = 0; $i < $rows; $i++) {
            $minutes = $i * 30;
            $row = new FarmData;
            $row->humidity = rand(10, 70);
            $row->temperature = rand(10, 40);
            $row->soil_moisture = rand(300, 500);
            $row->light = rand(0, 50);
            $row->datetime = date("Y-m-d H:i:s", strtotime("$start_date + $minutes minutes"));
            $row->save();
        }
    }

    public function actionData($type)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $data = HelperFarm::getStatistics($type);
            return $data;
        }
        return null;
    }

    public function actionFilterData($datetime_min, $datetime_max, $type)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $data = HelperFarm::getFilteredStatistics($datetime_min, $datetime_max, $type);
            return $data;
        }
        return null;
    }
}
