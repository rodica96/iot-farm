<?php

namespace frontend\controllers;

use common\models\FarmData;
use frontend\helpers\HelperFarm;
use Yii;
use yii\web\Controller;

/**
 * Graph Controller
 */
class ApiController extends Controller
{

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ["*"],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = 'json';
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionData()
    {
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $attributes = HelperFarm::convertFarmData($data);

            $farm_data = new FarmData;
            $farm_data->setAttributes($attributes);
            if ($farm_data->validate()) {
                $farm_data->save();
                return [
                    'status' => true,
                    'data' => $farm_data
                ];
            }
            return [
                'status' => false,
                'data' => $farm_data->getErrors()
            ];
        }
    }
}
