<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FarmDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="farm-list">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_actions') ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'datetime:datetime',
            'humidity',
            'temperature',
            'soil_moisture',
//            ['class' => 'yii\grid\ActionColumn'],
            'light',
        ],
    ]); ?>
</div>
