<?php

use yii\helpers\Html;
use yii\web\View;
use kartik\daterange\DateRangePicker;

/**
 * Created by PhpStorm.
 * User: rodyk
 * Date: 12/9/2018
 * Time: 12:54 PM
 */
?>

<div class="farm-graph">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-4">
            <h1 id="title"><?= $title ?></h1>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <?php
            echo DateRangePicker::widget([
                'name' => 'range',
                'startAttribute' => 'datetime_min',
                'endAttribute' => 'datetime_max',
                'convertFormat' => true,
                'pluginOptions' => [
                    'timePicker' => true,
                    'timePickerIncrement' => 30,
                    'locale' => [
                        'format' => 'Y-m-d H:i:s'
                    ]
                ]
            ]);
            ?>
        </div>
        <div class="col-md-1">
            <div id="filter-btn" class="btn btn-primary">Filter</div>
        </div>
    </div>
    <div class="clearfix"></div>

    <canvas id="canvas-<?= $type ?>" width="400" height="150"></canvas>

</div>

<?php
$script = <<< JS

const getColor = (type) => {
 const colors = {
  "humidity": "rgba(0, 128, 129, 0.3)",
  "temperature": "rgba(128,0,128, 0.3)",
  "soil_moisture": "rgba(240,128,128, 0.3)",
  "light": "rgba(255,215,0, 0.3)"
 };
 return colors[type];
};

const createChart = (container, label, bar, data) => {
 const myChart = new Chart(container, {
  type: 'line',
  data: {
   labels: bar,
   datasets: [{
    label,
    data,
    borderWidth: 1,
    backgroundColor: getColor("$type")
   }],
  },
  options: {
   scales: {
    yAxes: [{
     ticks: {
      beginAtZero: true
     }
    }]
   }
  }
 });
};

$.ajax({
 url: "data",
 type: "GET",
 dataType: "json",
 data: {type: "$type"},
 success: function(data) {
  createChart("canvas-$type", "$metric", data.datetime_bar, data.$type);
 },
 error: function(error) {
  console.log('error', error);
 }
});

$("#filter-btn").on('click', function() {
 const datetime_min = $('input[name="datetime_min"]').val();
 const datetime_max = $('input[name="datetime_max"]').val();
 if (datetime_min && datetime_max) {
  $.ajax({
   url: "filter-data",
   type: "GET",
   dataType: "json",
   data: { 
        datetime_min,
        datetime_max,
        type: "$type"
    },
   success: function(data) {
        const canvas = document.getElementById('canvas-$type');
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        createChart("canvas-$type", "$metric", data.datetime_bar, data.$type);
   },
   error: function(error) {
    console.log('error', error);
   }
  });
 }
});

JS;

$this->registerJs($script, View::POS_READY);
?>




