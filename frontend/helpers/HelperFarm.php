<?php

namespace frontend\helpers;

use common\models\FarmData;

/**
 * Created by PhpStorm.
 * User: rodyk
 * Date: 12/9/2018
 * Time: 1:13 PM
 */
class HelperFarm
{
    const COUNT_GRAPH_ROWS = 12;

    public static function getStatistics($type)
    {
        $statistics = FarmData::find()
            ->select("id, datetime, $type")
            ->orderBy('datetime desc')
            ->limit(self::COUNT_GRAPH_ROWS)
            ->asArray()
            ->all();
        return self::formatDataSingle(array_reverse($statistics), $type);
    }

    public static function getFilteredStatistics($datetime_min, $datetime_max, $type)
    {
        $statistics = FarmData::find()
            ->select("id, datetime, $type")
            ->where("datetime > '$datetime_min' and datetime < '$datetime_max'")
            ->orderBy('id asc')
            ->asArray()
            ->all();

        $total = count($statistics);
        if ($total <= self::COUNT_GRAPH_ROWS) {
            return self::formatDataSingle($statistics, $type);
        }
        $interval = $total / self::COUNT_GRAPH_ROWS;
        $final = [];
        $new_interval = 0;
        for ($i = 0; $i < self::COUNT_GRAPH_ROWS; $i++) {
            $last_interval = $new_interval;
            $new_interval = $last_interval + $interval;
            $avg_from = (int)$last_interval;
            $avg_to = (int)$new_interval;
            if ($i === self::COUNT_GRAPH_ROWS - 1) {
                $avg_to = $total - 1;
            }
            $avgs = FarmData::find()
                ->where('id >= ' . $statistics[$avg_from]['id'] . ' and id < ' . $statistics[$avg_to]['id'])
                ->average("$type");
            $final[] = [
                'datetime' => $statistics[$avg_from]['datetime'],
                "$type" => round($avgs, 2)
            ];
        }
        return self::formatDataSingle($final, $type);
    }

    public static function convertFarmData($data)
    {
        return [
            'humidity' => $data['hum1'],
            'temperature' => $data['temp1'],
            'soil_moisture' => $data['moisture'],
            'light' => $data['light'],
        ];
    }

    private static function formatDataSingle($statistics, $type)
    {
        $humidity = [];
        $temperature = [];
        $soil_moisture = [];
        $light = [];

        $datetime_bar = [];
        if (count($statistics)) {
            foreach ($statistics as $stat) {
                $datetime = self::formatDatetime($stat['datetime']);
                if ($type === 'humidity') {
                    $humidity[] = [
                        'x' => $datetime,
                        'y' => $stat['humidity']
                    ];
                }
                if ($type === 'temperature') {
                    $temperature[] = [
                        'x' => $datetime,
                        'y' => $stat['temperature']
                    ];
                }
                if ($type === 'soil_moisture') {
                    $soil_moisture[] = [
                        'x' => $datetime,
                        'y' => $stat['soil_moisture']
                    ];
                }
                if ($type === 'light') {
                    $light[] = [
                        'x' => $datetime,
                        'y' => $stat['light']
                    ];
                }
                $datetime_bar[] = $datetime;
            }
        }

        return [
            "$type" => $$type,
            'datetime_bar' => $datetime_bar
        ];
    }

    private static function formatDatetime($datetime)
    {
        return date("d/m H:i", strtotime($datetime));
    }
}