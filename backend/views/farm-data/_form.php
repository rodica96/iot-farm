<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FarmData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="farm-data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'humidity')->textInput() ?>

    <?= $form->field($model, 'temperature')->textInput() ?>

    <?= $form->field($model, 'soil_moisture')->textInput() ?>

    <?= $form->field($model, 'light')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
