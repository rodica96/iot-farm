<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FarmData */

$this->title = 'Create Farm Data';
$this->params['breadcrumbs'][] = ['label' => 'Farm Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="farm-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
